/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.passwordgen;

/**
 *
 * @author ASUS
 */
public class PasswordGen {

    private char[] upperList = new char[]{
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    };
    private char[] lowerList = new char[]{
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };
    private char[] numberList = new char[]{
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };

    private boolean[] requirement = new boolean[3];

    PasswordGen(boolean isReqUpper, boolean isReqLower, boolean isReqNumber) {
        requirement[0] = isReqUpper;
        requirement[1] = isReqLower;
        requirement[2] = isReqNumber;
    }

    String generate(int lengthPassword) {
        String password = "";

        for (int i = 0; i < lengthPassword; i++) {

            int selection = (int) Math.floor(Math.random() * 3);

            while (!requirement[selection]) {
                selection = (int) Math.floor(Math.random() * 3);
            }
            
            char randChar;
            
            if (selection == 0) {
                randChar = upperList[(int) Math.floor(Math.random() * 26)];
                password += randChar;
            }else if(selection == 1){
                randChar = lowerList[(int) Math.floor(Math.random() * 26)];
                password += randChar;
            } else if (selection == 2) {
                randChar = numberList[(int) Math.floor(Math.random() * 10)];
                password += randChar;
            }
            
        }
        
        return password;
    }
}