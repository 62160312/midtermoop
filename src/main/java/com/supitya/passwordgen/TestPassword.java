/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.passwordgen;

/**
 *
 * @author ASUS
 */
public class TestPassword {

    public static void main(String[] args) {

        PasswordGen machineOnlyNumber = new PasswordGen(false, false, true);

        PasswordGen machineOnlyUpperCase = new PasswordGen(true, false, false);

        PasswordGen machineOnlyLowerCase = new PasswordGen(false, true, false);

        PasswordGen machineUpperLower = new PasswordGen(true, true, false);
        
        // this is test every case .
        System.out.print("\nPassword Only Number : "    + machineOnlyNumber.generate(8));
        System.out.print("\nPassword Only Upper : "     + machineOnlyUpperCase.generate(8));
        System.out.print("\nPassword Only Lower : "     + machineOnlyLowerCase.generate(8));
        System.out.print("\nPassword Upper / Lower : "  + machineUpperLower.generate(8));
        
    }
}
